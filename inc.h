#ifndef _INC_H
#define _INC_H

#define _MINIX 1
#define _SYSTEM 1

#define LOCK 1
#define UNLOCK 2
#define MUTEXY_WAIT 3
#define BROADCAST 4
#define MUTEXY_SIGNAL 5
#define END_PROC 6

#define MUTEXES_NR  1024
#define CVS_NR      256
#define PROCS_NR    256
#define DUPLC -10
#define NON 15

#include <minix/callnr.h>
#include <minix/com.h>
#include <minix/config.h>
#include <minix/endpoint.h>
#include <minix/sysutil.h>
#include <minix/const.h>
#include <minix/type.h>
#include <minix/syslib.h>

#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <machine/vm.h>
#include <machine/vmparam.h>
#include <sys/vm.h>

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

EXTERN endpoint_t who_e;
EXTERN int call_type;

#endif
