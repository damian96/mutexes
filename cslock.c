#include <sys/cdefs.h>
#include <lib.h>
#include <unistd.h>
#include <errno.h>
#define _SYSTEM	1
#define _MINIX 1
#include <sys/cdefs.h>
#include <lib.h>
#include "namespace.h"
#include <minix/cs.h>
#include <minix/rs.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <errno.h>

static int get_ipc_endpt(endpoint_t *pt)
{
   return minix_rs_lookup("mutexy", pt);
}

int cs_lock(int id_mutex)
{
    while(1)
    {
    		endpoint_t pt;
    		get_ipc_endpt(&pt);
    		message m;
    		m.m_type = 1; // LOCK
    		m.m1_i1 = id_mutex;
    		int result = _syscall(pt, 1, &m);
    		// jesli proces oczekiwal na mutex, to ponawiam prosbe o mutex po obsludze sygnalu
    		if(errno==EINTR)
    			continue;
    		return result;
    }
}

int cs_unlock(int id_mutex)
{
    endpoint_t pt;
    get_ipc_endpt(&pt);
    message m;
    m.m_type = 2; // UNLOCK
    m.m1_i1 = id_mutex;
    return _syscall(pt, 2, &m);
}

int cs_wait(int cond_var, int id_mutex)
{
    endpoint_t pt;
    get_ipc_endpt(&pt);
    message m;
    m.m_type = 3; // WAIT
    m.m1_i1 = cond_var;
    m.m1_i2 = id_mutex;
    int result = _syscall(pt, 3, &m);
    // gdy oczekiwal na zdarzenie, to wykonuje cs_lock
    m.m_type = 1;
    m.m1_i1 = id_mutex;
    if(errno==EINTR) return _syscall(pt, 1, &m);
    else return result;
}

int cs_broadcast(int cond_var)
{
    endpoint_t pt;
    get_ipc_endpt(&pt);
    message m;
    m.m_type = 4; // BROADCAST
    m.m1_i1 = cond_var;
    return _syscall(pt, 4, &m);
}
