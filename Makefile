# Makefile for Mutex Server
PROG=	mutexy
SRCS=	main.c

DPADD+=	${LIBSYS}
LDADD+=	-lsys

MAN=

BINDIR?= /usr/sbin
FILES=mutex.conf
FILESNAME=mutexy
FILESDIR= /etc/system.conf.d

.include <minix.service.mk>
