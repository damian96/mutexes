#ifndef _CS_H
#define _CS_H

int cs_lock(int id_mutex);
int cs_unlock(int id_mutex);
int cs_wait(int cond_var, int id_mutex);
int cs_broadcast(int cond_var);

#endif
