#include "inc.h"

endpoint_t who_e;
int call_type, procSize, mutexSize, cvSize;

typedef struct {
  endpoint_t proc;
  int id_mutex;
  int next;
} procNode;

typedef struct {
  endpoint_t head;
  int index;
  int id_mutex;
  int index_last;
} list;

typedef struct {
  endpoint_t procArray[CVS_NR];
  int mutexes[CVS_NR];
  int cond_var;
  int size;
} cvar;

// Tablica z mutexami
list mutexes[MUTEXES_NR];
endpoint_t whoHasMutex[MUTEXES_NR] = {0};
int isUsed[MUTEXES_NR] = {0};
//tablica procesow, pole next odpowiada za kolejki mutexow
procNode procArray[PROCS_NR + MUTEXES_NR];
int isProcUsed[CVS_NR] = {0};
// tablica condition variables, dla kazdego osobna lista procesow zawieszonych
// w oczekiwaniu na odpowiednie cv
cvar condvarb[CVS_NR];
int isCvUsed[CVS_NR] = {0};

void createQue(endpoint_t proc, int id_mutex, int procIdx);
void createCvQue(int idx, int cond_var);
void add(int k, int id_mutex, endpoint_t proc);
void remove_mutex(endpoint_t proc, int k, int prev, int j);

int do_lock(int, endpoint_t);
int do_unlock(int, endpoint_t);
int do_wait(int, int, endpoint_t);
int do_broadcast(int, endpoint_t);
void send_sig(endpoint_t, endpoint_t);
void send_exit(endpoint_t, endpoint_t);

int main(int argc, char *argv[])
{
    message m;
    int mutex_id, cond_var, retFunc, result;
    endpoint_t proc;
    // rozmiary powyzszych list
    procSize = 0;
    mutexSize = 0;
    cvSize =/ 0;
    sef_startup();

  	while (TRUE) {
    		int r;

    	if ((r = sef_receive(ANY, &m)) != OK){
            printf("sef_receive failed %d.\n", r);
            continue;
        }

    	who_e = m.m_source;
    	call_type = m.m_type;
        switch (call_type) {
            case LOCK:
                mutex_id = m.m1_i1;
                retFunc = do_lock(mutex_id, who_e);
                if(retFunc == OK || retFunc == DUPLC){
          					m.m_type = 0;
          					result = send(who_e, &m);
          					if(result != OK) {
          						printf("ERROR SEND to %d", who_e);
          				  }
          			}
                break;

            case UNLOCK:
                mutex_id = m.m1_i1;
                retFunc = do_unlock(mutex_id, who_e);
                if(retFunc==OK){
                  m.m_type = 0;
                }
                else if(retFunc==EPERM){
                  m.m_type = -1;
                  errno = retFunc;
                }
                result = send(who_e, &m);
                if(result != OK) {
                  printf("ERROR SEND to %d", who_e);
                }
                break;

            case MUTEXY_WAIT:
                cond_var = m.m1_i1;
                mutex_id = m.m1_i2;
                retFunc = do_wait(cond_var, mutex_id, who_e);
                if(retFunc!=NON){
        				  if(retFunc==EINVAL){
        					 m.m_type = -1;
        					 errno = retFunc;
        				  }
        				  else
        					 m.m_type = 0;

                  result = send(who_e, &m);
                  if(result != OK) {
                    printf("ERROR SEND to %d", who_e);
                  }
                }
                break;

            case BROADCAST:
                cond_var = m.m1_i1;
                retFunc = do_broadcast(cond_var, who_e);
                m.m_type = 0;
                result = send(who_e, &m);
                if(result != OK) {
                  printf("ERROR SEND to %d", who_e);
                }
                break;

            case MUTEXY_SIGNAL:
        				// funkcja tell_mutexy_sig w pm/utility.c, wykorzystana w pm/signal.c/unpause
        				proc = m.PM_PROC;
        				send_sig(proc, who_e);
        				break;

      			case END_PROC:
        				// funkcja tell_mutexy_exit w pm/utility.c, wykorzystana w pm/forkexit.c/exit_proc
        				proc = m.PM_PROC;
        				send_exit(proc, who_e);
        				break;

            default:
				        continue;

        }
    }
  	return -1;
}

int do_lock(int id_mutex, endpoint_t proc)
{
  // wstawianie procesu do listy procesow, lista ma 256 + 1024 elementow, poniewaz
  // co  najwyzej 1024 mutexy sa zajete oraz co najwyzej 256 czeka na zdarzenia
  int j, k, l, currIdx=-1, procIdx=-1;
  for(l=0; l<procSize; l++)
  {
  	 if(isProcUsed[l]==0)
  	 {
      	procIdx = l;
      	break;
  	 }
  }
  if(procIdx == -1){
    	procIdx = procSize;
    	procSize++;
  }
  procArray[procIdx].proc = proc;
  procArray[procIdx].id_mutex = id_mutex;
  procArray[procIdx].next = -1;
  isProcUsed[procIdx] = 1;

  for(j=0; j<mutexSize; j++) {
    if(id_mutex == mutexes[j].id_mutex && isUsed[j]==1){
      currIdx = j;
      break;
    }
  }

  if(currIdx==-1){
	 for(j=0; j<mutexSize; j++){
		if(isUsed[j]==0){
			currIdx = j;
			break;
		}
	 }
  }

  // nikt nie zarezerwowal mutexu, wiec jest przydzielany procesowi
  if(currIdx==-1)
  {
    createQue(proc, id_mutex, procIdx);
    return OK;
  }
  else if(isUsed[currIdx]==0)
  {
  	 mutexes[currIdx].head = proc;
  	 mutexes[currIdx].index = procIdx;
  	 mutexes[currIdx].index_last = procIdx;
  	 mutexes[currIdx].id_mutex = id_mutex;
  	 isUsed[currIdx] = 1;
  	 whoHasMutex[currIdx] = proc;
  	 return OK;
  }
  else
  {
	// wstawianie procesu na koniec kolejki mutexa
    k = mutexes[currIdx].index;
	for(k = mutexes[currIdx].index; ; k = procArray[k].next)
	{
		if(k==-1)
			break;
		if(procArray[k].proc == proc)
			return DUPLC; // proces chce drugi raz ten sam mutex
	}

    j = mutexes[currIdx].index_last;
    procArray[j].next = procIdx;
    mutexes[currIdx].index_last = procIdx;
    return -100;
  }
}

int do_unlock(int id_mutex, endpoint_t proc)
{
  int j, k, tmp;
  for(j=0; j<mutexSize; j++)
  {
      if(id_mutex == mutexes[j].id_mutex && isUsed[j]==1)
      {
          if(mutexes[j].head == proc)
          {
      	      // jesli proces jest w posiadaniu mutexa, to go zwalniam i przydzielam nastepnemu w kolejce
              k = mutexes[j].index;
              if(procArray[k].next==-1)
              {
                isUsed[j] = 0;
              }
              else
              {
                tmp = procArray[k].next;
                mutexes[j].head = procArray[tmp].proc;
                mutexes[j].index = tmp;
                procArray[k].next = -1;
		            message m;
                m.m_type = 0;
				        send(procArray[tmp].proc, &m); // i wysylam wiadomosc do procesu, ktory dostaje mutex
              }
              isProcUsed[k] = 0;
              return 0;
          }
          else
          {
              return EPERM;
          }
      }
  }
  return EPERM;
}

int do_wait(int cond_var, int id_mutex, endpoint_t proc)
{
    int j, k, freeIdx=-1;
    int mutex = 0;
    for(j=0; j<mutexSize; j++)
    {
        if(mutexes[j].id_mutex == id_mutex && isUsed[j]==1)
        {
           if(mutexes[j].head == proc)
              mutex = 1;
        }
    }

    if(mutex==0)
    {
        return EINVAL;
    }
    else
    {
		// jesli proces jest w posiadaniu mutexa to go zwalniam oraz wrzucam do kolejki procesow
		// oczekujacych na zdarzenie cond_var ( funkcja add )
        do_unlock(id_mutex, proc);
        for(k=0; k<cvSize; k++)
        {
            if(freeIdx==-1 && isCvUsed[k]==0)
            {
                freeIdx = k;
            }

            if(condvarb[k].cond_var == cond_var && isCvUsed[k]==1)
            {
                add(k, id_mutex, proc);
                return NON;
            }
        }
        if(freeIdx==-1)
			     freeIdx = cvSize++;
        createCvQue(freeIdx, cond_var);
        add(freeIdx, id_mutex, proc);
        return NON;
    }
}

int do_broadcast(int cond_var, endpoint_t proc)
{
    int k, l;
    endpoint_t who_e;
    for(k=0; k<cvSize; k++)
    {
        if(condvarb[k].cond_var == cond_var && isCvUsed[k]==1)
        {
            for(l=0; l<condvarb[k].size; l++)
            {
				// procesy, ktore czekaly na zdarzenie cond_var trafiaja z powrotem do kolejek mutexow
				// jesli dostaja od razu mutex, to wysylam wiadomosc do tych procesow
                who_e = condvarb[k].procArray[l];
                int result = do_lock(condvarb[k].mutexes[l], who_e);
                if(result==OK)
                {
                  message m;
                  m.m_type = 0;
				          int res = send(who_e, &m);
                  if (res != OK)
                     printf("mutexy warning: send to %d failed!\n", who_e);
                }
            }
            condvarb[k].size = 0;
            isCvUsed[k] = 0;
        }
    }
    return OK;
}

void send_sig(endpoint_t proc, endpoint_t who_e)
{
	int j, k, l, prev = -1;
	for(j=0; j<mutexSize; j++)
	{
		if(isUsed[j]==1)
		{
			k = mutexes[j].index;
			for(k = mutexes[j].index; ; k = procArray[k].next)
			{
				if(k==-1)
					break;
				if(procArray[k].proc == proc)
				{
					message m;
					m.m_type = EINTR;
					errno = EINTR;
					int result = send(proc, &m);
					if (result != OK)
              printf("mutexy warning: send to %d failed!\n", proc);

					remove_mutex(proc, k, prev, j);
					return;
				}
				if(procArray[k].next != -1)
					prev = k;
			}
		}
	}

	for(j=0; j<cvSize; j++)
	{
		if(isCvUsed[j]==1)
		{
			for(k=0; k<condvarb[j].size; k++)
			{
				if(condvarb[j].procArray[k] == proc)
				{
  					message m;
  					m.m_type = EINTR;
  					errno = EINTR;
  					int result = send(proc, &m);
  					if(result != OK)
  						printf("mutexy warning: send to %d failed!\n", proc);

  					if(condvarb[j].size==1)
  					{
  						condvarb[j].size = 0;
  						isCvUsed[j] = 0;
  					}
  					else
  					{
  						for(l=k+1; l<condvarb[k].size; l++)
  						{
  							condvarb[j].procArray[l-1] = condvarb[j].procArray[l];
  							condvarb[j].mutexes[l-1] = condvarb[j].mutexes[l];
  						}
  						condvarb[j].size--;
  					}
  					break;
				}
			}
		}
	}
}

void send_exit(endpoint_t proc, endpoint_t who_e)
{
	int j, k, l, prev;
	for(j=0; j<mutexSize; j++)
	{
		// zwalniam mutexy zajete przez ten proces
		if(isUsed[j] && mutexes[j].head == proc)
			do_unlock(mutexes[j].id_mutex, proc);
		// usuwam go z kolejki mutexu
		if(isUsed[j]==1)
		{
			k = mutexes[j].index;
			for(k = mutexes[j].index; ; k = procArray[k].next)
			{
				if(k==-1)
					break;

				if(procArray[k].proc == proc){
					remove_mutex(proc, k, prev, j);
					break;
				}

				if(procArray[k].next != -1)
					prev = k;
			}
		}
	}

	for(j=0; j<cvSize; j++)
	{
		if(isCvUsed[j]==1)
		{
			for(k=0; k<condvarb[j].size; k++)
			{
				if(condvarb[j].procArray[k] == proc)
				{
					// usuwam proces z kolejki condition variable
					if(condvarb[j].size==1)
					{
						condvarb[j].size = 0;
						isCvUsed[j] = 0;
					}
					else
					{
						for(l=k+1; l<condvarb[k].size; l++)
						{
							condvarb[j].procArray[l-1] = condvarb[j].procArray[l];
							condvarb[j].mutexes[l-1] = condvarb[j].mutexes[l];
						}
						condvarb[j].size--;
					}
					break;
				}
			}
		}
	}
}

void createQue(endpoint_t proc, int id_mutex, int procIdx)
{
  mutexes[mutexSize].head = proc;
  mutexes[mutexSize].index = procIdx;
  mutexes[mutexSize].index_last = procIdx;
  mutexes[mutexSize].id_mutex = id_mutex;
  isUsed[mutexSize] = 1;
  whoHasMutex[mutexSize] = proc;
  mutexSize++;
}

void createCvQue(int idx, int cond_var)
{
  condvarb[idx].size = 0;
  condvarb[idx].cond_var = cond_var;
  isCvUsed[idx] = 1;
}

void add(int k, int id_mutex, endpoint_t proc)
{
  int tmp = condvarb[k].size++;
  condvarb[k].procArray[tmp] = proc;
  condvarb[k].mutexes[tmp] = id_mutex;
}

void remove_mutex(endpoint_t proc, int k, int prev, int j)
{
	if(procArray[k].next==-1 && prev==-1)
	{
		isUsed[j] = 0;
		return;
	}
	else
	{
		if(prev == -1)
		{
			mutexes[j].index = procArray[k].next;
			mutexes[j].head = procArray[mutexes[j].index].proc;
			if(procArray[mutexes[j].index].next == -1)
				mutexes[j].index_last = mutexes[j].index;
		}
		else if(procArray[mutexes[j].index_last].proc == procArray[k].proc)
		{
			procArray[prev].next = -1;
			mutexes[j].index_last = prev;
		}
		else
		{
			procArray[prev].next = procArray[k].next;
		}
	}
	procArray[k].next = -1;
	isProcUsed[k] = 0;
}
